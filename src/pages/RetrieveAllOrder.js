import { useContext, useState, useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import UserContext from "../UserContext";




export default function RetrieveAllOrder(){

    const { user } = useContext(UserContext);

    const [ allOrders, setallOrders ] = useState([]);

    const fetchOrders = () => {

        fetch(`http://localhost:4000/orders/allorders`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setallOrders(data.map(order => {
                return (
                    <tr key={order._id}>
                        <td>{order._id}</td>
                        <td>{order.userId}</td>
                        <td>{order.totalAmount}</td>
                        <td>{order.purchasedOn}</td>
                    </tr>
                )
            }))
        })
    }


    useEffect(()=>{

        fetchOrders();
    }, [])

    return (
        (user.isAdmin)
                ?
                <>
                    <div className="mt-5 mb-3 text-center">
                        <h1>Admin Dashboard</h1>
                        <Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
                        <Button as={Link} to="/allOrders" variant="primary" size="lg" className="mx-2">Show Orders</Button>
                        <Button as={Link} to="/allUsers" variant="primary" size="lg" className="mx-2">All Users</Button>
                    </div>
                    <Table striped bordered hover>
                     <thead>
                       <tr>
                         <th>Order ID</th>
                         <th>User ID</th>
                         <th>Total Amount</th>
                         <th>Purchase Date</th>
                       </tr>
                     </thead>
                     <tbody>
                       { allOrders }
                     </tbody>
                   </Table>
                </>
                :
                <Navigate to="/viewProducts" />

    )

}
