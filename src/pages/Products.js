import { useState,useEffect } from 'react';
import ProductCard from '../components/ProductCard'

export default function Products(){

	const [productDetails, setProductDetails] = useState([])

	useEffect(() => {
		fetch(`http://localhost:4000/products/`)
		.then(res => res.json())
		.then(data => {

			setProductDetails(data.map(product => {
				return (
					<ProductCard prodProp={product}/>
				)
			}))	
		})

},[])

	return (
		<>
			<h1 className="my-5 text-center">Available Products</h1>
			{productDetails}
		</>
		)
}
