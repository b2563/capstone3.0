import { useContext, useState, useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function AdminDashboard(){

	const { user } = useContext(UserContext);


	const [ allProducts, setAllProducts ] = useState([]);

	const fetchProducts = () => {

		fetch(`http://localhost:4000/products/all`, {
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{ product.isActive ? "Active" : "Inactive"}</td>
						<td>
								{
									(product.isActive)
									? <Button variant="secondary" size="sm" onClick = {() => productArchive(product._id, product.name)}>Archive</Button>
									: <>
										<Button variant="primary" size="sm" onClick ={() => activateProduct(product._id, product.name)}>Unarchive</Button>
										<Button as={ Link } to={`/update/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
									  </>
								}
						</td>
					</tr>
				)
			}))
		})
	}


	const productArchive = (productId, productName) => {
		console.log(productId);
		console.log(productName);

		fetch(`http://localhost:4000/products/archive/${productId}`, {
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Product Archived!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchProducts();
			} else {
				Swal.fire({
					title: "Product Archived Unsuccesful!",
					icon: "error",
					text: `Something went wrong. Please try again later.`
				})
			}
		})
	}

	const activateProduct = (productId, productName) => {
		console.log(productId);
		console.log(productName);
		
		fetch(`http://localhost:4000/products/activateProduct/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchProducts();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})

	}

	useEffect(()=>{

		fetchProducts();
	}, [])

	return (
		(user.isAdmin)
				?
				<>
					<div className="mt-5 mb-3 text-center">
						<h1>Admin Dashboard</h1>
						<Button as={Link} to="/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
						<Button as={Link} to="/allOrders" variant="primary" size="lg" className="mx-2">Show Orders</Button>
						<Button as={Link} to="/" variant="primary" size="lg" className="mx-2">All Users</Button>
					</div>
					<Table striped bordered hover>
				     <thead>
				       <tr>
				         <th>Product ID</th>
				         <th>Product Name</th>
				         <th>Description</th>
				         <th>Price</th>
				         <th>Status</th>
				         <th>Action</th>
				       </tr>
				     </thead>
				     <tbody>
				       { allProducts }
				     </tbody>
				   </Table>
				</>
				:
				<Navigate to="/viewProducts" />

	)

}
